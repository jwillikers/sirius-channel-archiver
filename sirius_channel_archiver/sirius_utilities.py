from pysosirius.sirius_channel import SiriusChannel
from pysosirius.sirius_playing import SiriusCurrentlyPlaying
from pysosirius.pysosirius import PySoSirius
from pysosirius.web.utilities import SiriusScraper
from pysosirius.database.manager import SiriusManager
import selenium.common.exceptions

import logging


log = logging.getLogger(__name__)


def get_channel_from_db(channel):
    sirius = PySoSirius()
    manager = SiriusManager()

    if isinstance(channel, int):
        channel_row = manager.get_channel_row(channel=channel)
    else:
        channel_row = manager.get_channel_row(name=channel)

    if channel_row is None:
        log.debug(
            'Channel {} not found in the local database.'.format(channel))
        sirius_channel = None
    else:
        log.debug('Channel {} found in the local database.'.format(channel))
        channel_data = PySoSirius.wrap_channel_data(channel_row)
        sirius_channel = SiriusChannel(
            channel_data.channel,
            channel_data.id,
            channel_data.name,
            channel_data.url)

    return sirius_channel


def get_channel_from_web(channel):
    scraper = SiriusScraper()
    found = True
    try:
        if isinstance(channel, int):
            scraped_channel = scraper.get_channel_data(
                channel=channel)
        else:
            scraped_channel = scraper.get_channel_data(
                channel_name=channel)
    except selenium.common.exceptions.NoSuchElementException:
        found = False
    if found and scraped_channel is None:
        found = False
    if found:
        channel_data = PySoSirius.wrap_channel_data(scraped_channel)
        log.debug('Found channel data {}.'.format(channel_data))
        log.info('Found new channel {}.'.format(channel_data.name))
        manager = SiriusManager()
        manager.set_channel_row(channel_data)
        log.info('Stored channel {} in the local database.'.format(
            channel_data.name))
        manager.save()
        sirius_channel = SiriusChannel(
            channel_data.channel,
            channel_data.id,
            str(channel_data.name, 'utf-8'),
            channel_data.url)
    else:
        sirius_channel = None
        log.info(
            'Could not find channel {} on SiriusXM''s website.'.format(
                channel))
    scraper.browser.quit()
    return sirius_channel


def get_channel(channel):
    sirius_channel = get_channel_from_db(channel)
    if sirius_channel is None:
        sirius_channel = get_channel_from_web(channel)
    return sirius_channel
