import signal
import sirius_utilities
import logging
import timer
from channel_spy import ChannelSpy
from song_file_writer import SongFileWriter


class ChannelArchiverService(object):

    def __init__(
            self,
            channel, output_filename=None,
            number_of_songs=-1,
            timeout=-1,
            cache_size=15):
        self.log = logging.getLogger(
            'sirius_channel_archiver.ChannelArchiverService')
        self.number_of_songs = number_of_songs
        self.timeout = timeout * 60 * 60
        sirius_channel = sirius_utilities.get_channel(channel)
        self.spy = ChannelSpy(sirius_channel)
        if not output_filename:
            output_filename = sirius_channel.name + '.csv'
        self.writer = SongFileWriter(output_filename)
        self.shutting_down = False
        self.timer = timer.Timer()
        self.song_count = 0

    def run(self):
        self.shutting_down = False
        self.log.info('Channel Archiver Service is starting.')
        signal.signal(signal.SIGINT, self.signal_handler)
        while not self.shutting_down and self.should_continue():
            try:
                song = self.spy.get_next()
            except KeyboardInterrupt:
                self.log.warn(
                    'Received keyboard interrupt while running.')
                break
            self.writer.write(song)
            self.update_song_count()
        self.shutdown()

    def shutdown(self):
        self.log.info('Shutting down.')
        self.shutting_down = True
        self.spy.shutdown()
        self.writer.shutdown()

    def signal_handler(self, signal, frame):
        self.log.warn('You pressed Ctrl+C.')
        self.shutdown()

    def out_of_time(self):
        if self.timeout > 0:
            out = self.timer.time() >= self.timeout
        else:
            out = False
        return out

    def still_time(self):
        if self.timeout > 0:
            still = self.timer.time() < self.timeout
        else:
            still = True
        return still

    def out_of_songs(self):
        if self.number_of_songs > 0:
            out = self.song_count >= self.number_of_songs
        else:
            out = False
        return out

    def still_songs(self):
        if self.number_of_songs > 0:
            still = self.song_count < self.number_of_songs
        else:
            still = True
        return still

    def should_continue(self):
        return self.still_time() and self.still_songs()

    def update_song_count(self):
        if self.number_of_songs:
            self.song_count += 1
            self.log.info(
                'Retrieved {} songs.'.format(
                    self.song_count))
