

class Song:

    def __init__(self, name, artist, timestamp):
        self.name = str(name)
        self.artist = str(artist)
        self.timestamp = timestamp

    @classmethod
    def from_currently_playing(cls, currently_playing):
        return Song(
            currently_playing.song_name,
            currently_playing.artist_name,
            currently_playing.start)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            equal = \
                other.name == self.name and \
                other.artist == self.artist \
                and other.timestamp == self.timestamp
        else:
            equal = False
        return equal

    def __hash__(self):
        return hash(self.name + self.artist + self.timestamp)

    def to_dict(self):
        return {
            'song': self.name,
            'artist': self.artist,
            'timestamp': self.timestamp}

    def is_incomplete(self):
        if self.name is None or self.name == '':
            empty = True
        elif self.artist is None or self.artist == '':
            empty = True
        elif self.timestamp is None or self.timestamp == '':
            empty = True
        else:
            empty = False
        return empty

    def is_empty(self):
        return (self.name is None or self.name == '') and \
            (self.artist is None or self.artist == '') and \
            (self.timestamp is None or self.timestamp == '')

    def is_complete(self):
        return not self.is_empty()

    def is_ch_ad(self):
        ch_ad = self.name[:3] == 'Ch.'
        if not ch_ad:
            try:
                ch_ad = self.name[:2] == 'Ch' and int(self.name[2:])
            except ValueError:
                ch_ad = False
        return ch_ad

    def has_ad_name(self):
        return '/' in self.name and '.com' in self.name

    def has_ad_artist(self):
        return '@' in self.artist

    def is_ad(self):
        return self.has_ad_name() or self.has_ad_artist() or self.is_ch_ad()

    def is_valid(self):
        return not self.is_ad() and self.is_complete()

    def is_invalid(self):
        return self.is_incomplete() or self.is_ad()

    def __str__(self):
        return '{} by {}'.format(self.name, self.artist)

    def __repr__(self):
        return '{} by {} played at {}'.format(
            self.name, self.artist, self.timestamp)
