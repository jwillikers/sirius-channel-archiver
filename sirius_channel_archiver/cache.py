

class Cache(object):

    def __init__(self, size=15):
        self.size = size
        self.objects = list()

    def is_full(self):
        return len(self.objects) == self.size

    def has_space(self):
        return len(self.objects) < self.size

    def is_empty(self):
        return len(self.objects) == 0

    def has_at_least_one(self):
        return len(self.objects) >= 1

    def store(self, obj):
        if self.has_space():
            self.objects.append(obj)
            stored = True
        else:
            stored = False
        return stored

    def clear(self):
        self.objects.clear()

    def retrieve(self):
        tmp = list.copy(self.objects)
        self.clear()
        return tmp
