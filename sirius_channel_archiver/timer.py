import time


class Timer(object):

    def __init__(self):
        self.start_time = time.time()

    def reset(self):
        self.timer = time.time()

    def time(self):
        return time.time() - self.start_time
