from pysosirius.sirius_channel import SiriusChannel
from pysosirius.sirius_playing import SiriusCurrentlyPlaying
import logging
import threading


class ChannelSpy(object):
    """Monitors the songs on air on a SiriusXM Radio channel."""

    INVALID_SONG_WAIT_TIME = 20
    SONG_WAIT_TIME = 60

    def __init__(self, channel):
        self.log = logging.getLogger('sirius_channel_archiver.ChannelSpy')
        self.channel = channel
        self.current_song = None
        self.running = True
        self.wait_event = threading.Event()
        self.wait_thread = threading.Thread(target=self.get_current)
        self.wait_thread.start()

    def is_running(self):
        """Determine if the ChannelSpy is currently running."""
        return self.running

    def is_shutting_down(self):
        """Determine if the ChannelSpy is currently shutting down."""
        return not self.running

    def _get_current(self):
        """Determine the song or ad currently playing."""
        from song import Song
        return Song.from_currently_playing(
            self.channel.get_currently_playing())

    def get_current(self):
        """Determine the song currently playing."""
        current = self._get_current()
        while self.is_running() \
                and current.is_invalid() \
                and not self.wait_event.is_set():
            self.log.info(
                '''Currently playing is not a song. Will retry in {} seconds.
                '''.format(
                    ChannelSpy.INVALID_SONG_WAIT_TIME))
            self.wait_event.wait(ChannelSpy.INVALID_SONG_WAIT_TIME)
            current = self._get_current()
        return current

    def update_current(self, song=None):
        """Update the currently playing song for this ChannelSpy.

        Args:
            song (Song): the song to update to.
        """
        if song:
            self.current_song = song
        else:
            self.current_song = self.get_current()
        self.log.info(
            'Currently playing {}.'.format(
                song))

    def get_next(self):
        """Retrieve the next song that plays on the channel."""
        next_song = self.get_current()
        while self.is_running() \
                and self.current_song == next_song \
                and not self.wait_event.is_set():
            self.log.info(
                '{} is still playing. Sleeping for {} seconds.'.format(
                    next_song,
                    ChannelSpy.SONG_WAIT_TIME))
            self.wait_event.wait(ChannelSpy.SONG_WAIT_TIME)
            next_song = self.get_current()
        self.update_current(song=next_song)
        return next_song

    def shutdown(self):
        """Stop this ChannelSpy from running."""
        if self.is_running():
            self.running = False
            self.wait_event.set()
            self.wait_thread.join()
            self.log.info('Shutting down.')
