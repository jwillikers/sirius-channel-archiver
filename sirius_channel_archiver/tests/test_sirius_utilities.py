import unittest
import unittest.mock
from unittest.mock import patch
from pysosirius.web.utilities import SiriusScraper
from pysosirius.database.manager import SiriusManager
import selenium.common.exceptions
from selenium import webdriver
import sirius_utilities


class TestGetChannelFromWeb(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.patchers = [
            patch.object(webdriver.Firefox, 'get', return_value=None),
            patch.object(webdriver.Firefox, 'add_cookie', return_value=None),
            patch.object(
                webdriver.Firefox,
                'find_element_by_id',
                return_value=None),
            patch.object(webdriver.Firefox, '__init__', return_value=None),
            patch.object(webdriver.Firefox, 'quit', return_value=None)]
        for patcher in cls.patchers:
            patcher.start()

    @classmethod
    def tearDownClass(cls):
        for patcher in cls.patchers:
            patcher.stop()

    def test_channel_name(self):
        with patch.object(
            SiriusScraper,
            'get_channel_data',
            return_value=(
                53,
                'chill',
                b'SiriusXM Chill', 'https://www.siriusxm.com/chill',
                'Music',
                'Dance/Electronic',
                'Downtempo/Deep House')) as mock_get_channel_data:
            channel = sirius_utilities.get_channel_from_web('SiriusXM Chill')
            mock_get_channel_data.assert_called_with(
                channel_name='SiriusXM Chill')
        self.assertEqual(
            channel.channel,
            53,
            msg='The id of the Channel is not correct.')
        self.assertEqual(
            channel.name,
            'SiriusXM Chill',
            msg='The name of the Channel is not correct.')

    def test_channel_id(self):
        channel_name = 'SiriusXM Chill'
        channel_id = 53
        with patch.object(
            SiriusScraper,
            'get_channel_data',
            return_value=(
                53,
                'chill',
                b'SiriusXM Chill', 'https://www.siriusxm.com/chill',
                'Music',
                'Dance/Electronic',
                'Downtempo/Deep House')) as mock_get_channel_data:
            channel = sirius_utilities.get_channel_from_web(channel_id)
            mock_get_channel_data.assert_called_with(channel=channel_id)
        self.assertEqual(
            channel.channel,
            channel_id,
            msg='The id of the Channel is not correct.')
        self.assertEqual(
            channel.name,
            channel_name,
            msg='The name of the Channel is not correct.')

    def test_nonexistant_channel_id(self):
        with patch.object(
            SiriusScraper,
            'get_channel_data',
                side_effect=selenium.common.exceptions.NoSuchElementException):
            channel = sirius_utilities.get_channel_from_web(99999)
        self.assertIsNone(channel)

    def test_nonexistant_channel_name(self):
        with patch.object(
            SiriusScraper,
            'get_channel_data',
                side_effect=selenium.common.exceptions.NoSuchElementException):
            channel = sirius_utilities.get_channel_from_web(
                'Doofenshmirtz Evil Incorporated')
        self.assertIsNone(channel)


class TestGetChannelFromDb(unittest.TestCase):

    def test_channel_name(self):
        with patch.object(
            SiriusManager,
            'get_channel_row',
            return_value=(
                25,
                'classicrewind',
                'Classic Rewind', 'https://www.siriusxm.com/classicrewind',
                'Music',
                'Rock',
                "'70s/'80s Classic Rock")) as mock_get_channel_row:
            channel = sirius_utilities.get_channel_from_db('Classic Rewind')
            mock_get_channel_row.assert_called_with(name='Classic Rewind')
        self.assertEqual(
            channel.name,
            'Classic Rewind',
            msg='The name of the Channel is not correct.')
        self.assertEqual(
            channel.channel,
            25,
            msg='The id of the Channel is not correct.')

    def test_channel_id(self):
        with patch.object(
            SiriusManager,
            'get_channel_row',
            return_value=(
                25,
                'classicrewind',
                'Classic Rewind', 'https://www.siriusxm.com/classicrewind',
                'Music',
                'Rock',
                "'70s/'80s Classic Rock")) as mock_get_channel_row:
            channel = sirius_utilities.get_channel_from_db(25)
            mock_get_channel_row.assert_called_with(channel=25)
        self.assertEqual(
            channel.name,
            'Classic Rewind',
            msg='The name of the Channel is not correct.')
        self.assertEqual(
            channel.channel,
            25,
            msg='The id of the Channel is not correct.')

    def test_nonexistant_channel_id(self):
        with patch.object(
            SiriusManager,
            'get_channel_row',
                return_value=None) as mock_get_channel_row:
            channel = sirius_utilities.get_channel_from_db(99999)
            mock_get_channel_row.assert_called_with(channel=99999)
        self.assertEqual(channel, None)

    def test_nonexistant_channel_name(self):
        with patch.object(
            SiriusManager,
            'get_channel_row',
                return_value=None) as mock_get_channel_row:
            channel = sirius_utilities.get_channel_from_db(
                'Doofenshmirtz Evil Incorporated')
            mock_get_channel_row.assert_called_with(
                name='Doofenshmirtz Evil Incorporated')
        self.assertEqual(channel, None)
