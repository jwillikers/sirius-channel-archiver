import unittest
from song import Song


class SongTestCase(unittest.TestCase):

    def test_is_ch_ad(self):
        ch_ad1 = Song(
            'Ch. 10',
            '',
            '2018-05-12T12:32:48Z')
        self.assertTrue(
            ch_ad1.is_ch_ad(), msg='Ch. 10 ad is not caught')
        ch_ad2 = Song(
            'Ch102',
            '',
            '2018-05-12T12:32:48Z')
        self.assertTrue(
            ch_ad2.is_ch_ad(), msg='Ch102 ad is not caught')
        not_ch_ad1 = Song(
            'Channel',
            '',
            '2018-05-12T12:32:48Z')
        self.assertFalse(
            not_ch_ad1.is_ch_ad(), msg='Channel is caught')

    def test_has_ad_name(self):
        ad1 = Song(
            'fb.com/test',
            '',
            '2018-05-12T12:32:48Z')
        self.assertTrue(
            ad1.has_ad_name(), msg='fb.com/test is not caught')
        not_ad1 = Song(
            'Telephone Line',
            'Electric Light Orchestra',
            '2018-05-12T12:32:48Z')
        self.assertFalse(
            not_ad1.has_ad_name(), msg='actual song is marked as ad')

    def test_constructor_types(self):
        one_hundred = Song(100, 'Game', '2018-05-12T12:32:48Z')
        self.assertIsInstance(
            one_hundred.name,
            str,
            msg='Song name should be of type string')
        three_eleven = Song('Down', 311, '2018-05-12T12:32:48Z')
        self.assertIsInstance(
            three_eleven.artist,
            str,
            msg='Song artist should be of type string')
