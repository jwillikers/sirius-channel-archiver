from unittest import TestCase
from unittest.mock import patch
from song import Song
from channel_spy import ChannelSpy
from pysosirius.sirius_channel import SiriusChannel
import time


class ChannelSpyTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.channel = SiriusChannel(
            25,
            'classicrewind',
            'Classic Rewind',
            'https://www.siriusxm.com/classicrewind')
        self.ad1 = Song(
            'Ch103',
            'Volume',
            '2018-05-12T12:25:48Z')
        self.song1 = Song(
            'Owner Of A Lonely Heart',
            'Yes',
            '2018-05-12T12:25:48Z')
        self.song2 = Song(
            'Hey Nineteen',
            'Steely Dan',
            '2018-05-12T12:28:48Z')
        self.song3 = Song(
            'Telephone Line',
            'Electric Light Orchestra',
            '2018-05-12T12:32:48Z')
        self.songs = [self.ad1, self.song1, self.song2, self.song3]
        self.currently_playing_patcher = patch.object(
            Song,
            'from_currently_playing',
            side_effect=self.songs)
        self.spy = ChannelSpy(self.channel)
        ChannelSpy.INVALID_SONG_WAIT_TIME = 0
        ChannelSpy.SONG_WAIT_TIME = 0
        self.currently_playing_patcher.start()

    def tearDown(self):
        self.currently_playing_patcher.stop()
        self.spy.shutdown()

    def test_get__current(self):
        playing = self.spy._get_current()
        self.assertEqual(playing, self.ad1)
        playing = self.spy._get_current()
        self.assertEqual(playing, self.song1)
        playing = self.spy._get_current()
        self.assertEqual(playing, self.song2)
        playing = self.spy._get_current()
        self.assertEqual(playing, self.song3)

    def test_get_current(self):
        playing = self.spy.get_current()
        self.assertEqual(playing, self.song1)
        playing = self.spy._get_current()
        self.assertEqual(playing, self.song2)
        playing = self.spy._get_current()
        self.assertEqual(playing, self.song3)

    def test_update_current(self):
        self.spy.update_current()
        self.assertEqual(self.spy.current_song, self.song1)
        self.spy.update_current(self.song3)
        self.assertEqual(self.spy.current_song, self.song3)

    def test_get_next(self):
        next_song = self.spy.get_next()
        self.assertEqual(next_song, self.song1)
        next_song = self.spy.get_next()
        self.assertEqual(next_song, self.song2)
        next_song = self.spy.get_next()
        self.assertEqual(next_song, self.song3)


if __name__ == '__main__':
    unittest.main()
