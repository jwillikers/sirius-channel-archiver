from csv import DictWriter
from song import Song
from cache import Cache
import logging
import time
import threading


class SongFileWriter(object):

    def __init__(self, output_filename, cache_size=15, cache_timeout=900):
        self.log = logging.getLogger('sirius_channel_archiver.SongFileWriter')
        self.output_filename = output_filename
        self.cache = Cache(cache_size)
        self.cache_size = cache_size
        self.create()
        self.cache_timeout = cache_timeout
        self.timeout_event = threading.Event()
        self.cache_timeout_thread = threading.Thread(
            target=self.write_cache_timeout)
        self.cache_timeout_thread.start()
        self.running = True

    def is_running(self):
        return self.running

    def is_shutting_down(self):
        return not self.running

    def create(self):
        with open(self.output_filename, mode='w') as csvfile:
            writer = DictWriter(csvfile, ['song', 'artist', 'timestamp'])
            writer.writeheader()
        self.log.info('Created csv file {}.'.format(self.output_filename))

    def write(self, song):
        if self.cache.is_full():
            self.write_to_file()
        return self.cache.store(song)

    def write_to_file(self):
        self.log.info('Writing cache to file.')
        with open(self.output_filename, mode='a') as csvfile:
            writer = DictWriter(csvfile, ['song', 'artist', 'timestamp'])
            cached = self.cache.retrieve()
            for song in cached:
                writer.writerow(song.to_dict())

    def write_cache_timeout(self):
        while not self.timeout_event.is_set():
            if self.cache.has_at_least_one() > 0:
                self.write_to_file()
            self.timeout_event.wait(self.cache_timeout)

    def shutdown(self):
        if self.is_running():
            self.running = False
            self.log.info('Shutting down.')
            self.timeout_event.set()
            self.cache_timeout_thread.join()
            self.write_to_file()
