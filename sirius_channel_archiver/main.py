

def main():
    import sys
    import logging
    import argparse
    from channel_archiver_service import ChannelArchiverService
    parser = argparse.ArgumentParser(
        prog='Sirius Channel Archiver',
        usage='''
              python3 main.py channel -o
              /path/to/output.csv -n 100 -t 5''',
        description='''
                    Archive song title and artist for a certain number of songs
                    from a SiriusXM radio channel and output to a csv.''',
        epilog=None)
    parser.add_argument(
        'channel',
        help='''
            id or name of the channel, enclosing it in quotations if
            spaces are present''')
    parser.add_argument(
        '-o',
        '--output',
        help='output file')
    parser.add_argument(
        '-n',
        '--number_of_songs',
        type=int,
        default=-1,
        help='number of songs to archive before quitting')
    parser.add_argument(
        '-t',
        '--timeout',
        type=int,
        default=-1,
        help='the amount of time to run before stopping in hours')
    parser.add_argument(
        '-i',
        '--info',
        action='store_true',
        help='enable info level logging')
    parser.add_argument(
        '-d',
        '--debug',
        action='store_true',
        help='enable debug logging')
    args = parser.parse_args()

    if args.channel.isdigit():
        args.channel = int(args.channel)

    if args.debug:
        log_level = logging.DEBUG
    elif args.info:
        log_level = logging.INFO
    else:
        log_level = logging.ERROR
    logging.basicConfig(level=log_level)

    service = ChannelArchiverService(
        args.channel,
        args.output,
        args.number_of_songs,
        args.timeout)
    service.run()
    sys.exit(0)


if __name__ == '__main__':
    main()
