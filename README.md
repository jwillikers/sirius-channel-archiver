Sirius Channel Archiver
===
Record the song name and artist name for songs on a given Sirius radio channel.

Getting Started
===
The following are the instructions for installing Sirius Channel Archiver and its dependencies.

Prerequisites
---
-   python3
-   geckodriver

### Installing Prerequisites OSX
#### Using the ubiquitous Homebrew
```console
brew install python3
```
```console
brew install geckodriver
```

#### Using the even more ubiquitous inter-webs
-   Download the installer for python3 and follow the instructions.
-   Download the shell installer for geckodriver and run it.
```console
sh geckodriver-install.sh
```

Installing
---
Installing the script is barebones at this point. Download or clone the repo and go to where the requirements.txt file lies: sirius_channel_archiver/sirius_channel_archiver. Then install the requirements for python using pip.

The following assumes this is in a folder called Downloads under the user's home folder.
```console
cd ~/Downloads/sirius_channel_archiver
pip3 install -r requirements.txt
```

Updating
---
The repository can be downloaded by re-downloading it or changing into the director and running the git command pull. Please note git must be installed. The following shows updating via git.
```console
cd ~/Downloads/sirius_channel_archiver
git pull
```
Note that if the dependencies of the script have changed, the requirements will have to be installed. As above, and shown again below, use pip. Ensure you are in the script's directory as usual.
```console
pip3 install -r requirements.txt
```

Usage
---
These are instructions on using the script. The script will run in the foreground until completion and only writes its cache to file after every 15 songs or less in the case the user specifies less than 15 songs or the timeout is reached before 15 songs are cached. This means you may not see anything in the file for a while.

The script is just inside the sirius_channel_archiver/sirius_channel_archiver folder. Make sure you provide the full path to the script or run the script from inside this folder, as in the examples below.

The script is straightforward to use and includes command line help using the -h option.
```python
python3 main.py -h
```

To gather the song data for a channel, include the channel name or number. When giving the name, make sure to surround the name with quotes if there are spaces. This will collect information for songs forever, storing them in a csv file with the same name as the channel. The following collects songs for the channel called SiriusXM Chill.
```python
python3 main.py "SiriusXM Chill"
```

This also collects songs for the channel SiriusXM Chill, but uses the channel id instead of the name. Channel id's are assigned by SiriusXM and can be found online on the [SiriusXM Channel Lineup](https://www.siriusxm.com/channellineup).
```python
python3 main.py 53
```

To change the number of songs recorded, use the -n, or --number_of_songs, option. The below example collects the data for 100 songs before quitting.
```python
python3 main.py "Classic Rewind" -n 100
```

To have the script stop after a certain amount of time, use the -t, or --timeout, option, which takes a number of hours. Following is an example where the script runs for 2 hours before quitting.
```python
python3 main.py 53 -t 2
```

The number_of_songs and timeout options can be combined, in which case the script will stop when the first condition is met. So, in the following a example, the script stops whenever an hour is reached or 10 songs are recorded, whichever happens first.
```python
python3 main.py "Venus" -t 1 -n 10
```

To change the name of the output file or its path, use the -o, or --output, option and provide the name or path to the file, where it will be created or overwritten in the case it already exists. The below creates the file in the relative directory Downloads under the name songs_and_artists.csv. Please include the file extension.
```python
python3 main.py "Pitbull's Globalization" -o Downloads/songs_and_artists.csv
```

If you want to see what is going on while the script is running, some logging is included and can be enabled with the -i, --info, option.
```python
python3 main.py "The Coffee House" -i
```

If you want to really see what is going on while the script is running, debug logging is included and can be enabled with the -d, --debug, option.
```python
python3 main.py 15 -d
```


Authors
-------
-   Jordan Williams

License
-------
This project is licensed under the MIT license.
